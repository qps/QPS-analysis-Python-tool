""" Example of the use of I2C communication with nDQQDI
    Author: Alexandre Plot and Severin Haas
    Date: 21/06/2019 14:30
"""

from pyftdi.i2c import I2cController
from CommunicationProtocol import COMi2c


FTDI_URL = 'ftdi://ftdi:232h/1'
print(FTDI_URL)

# Instanciate an I2C controller
I2C = I2cController()

# Configure the interface of the FTDI device as an I2C master
I2C.configure(FTDI_URL)
print("Device opened")

# Get a port to an I2C slave device
SLAVE = I2C.get_port(0x11)


""" Instanciate an I2C communication with nDQQDI and the communication
    protocol class
"""
DQQDI = COMi2c(SLAVE)

# read register 20
print(DQQDI.read_register(20, 1))

# modify register 20 with value 122
DQQDI.write_register(20, 122)

print(DQQDI.read_register(20, 1))

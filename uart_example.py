""" Example of the use of UART communication with uqds channel
    Author: Alexandre Plot (aplot) and Severin Haas
    Date: 21/06/2019 14:30
"""

import time
import serial
import matplotlib.pyplot as plt
from CommunicationProtocol import COMuart


# Define the address for reading the buffer
ADRESS_START = 112
ADRESS_STOP = 115
DUMMY_BYTES = [0, 0, 0, 0, 0]
REPETITIONS = 128
NUMBER_REGISTERS = (ADRESS_STOP - ADRESS_START) + 1
BLOCKS = 128

# Define the COM port to use (see device manager setting on windows)
COM = 'COM10'

# Create port for communication
SER = serial.Serial()

# Instanciate an UART communication object with uqds
UQDS = COMuart(SER, COM)

# Changing the gain
print(UQDS.read_register(24, 5))
#UQDS.writeRegister(24, [0,0x04,0,0])

time.sleep(2)
print(UQDS.read_register(24, 5))

# Read the buffer and plot it
buffer = UQDS.read_buffer(NUMBER_REGISTERS, REPETITIONS, BLOCKS, \
                          ADRESS_START, ADRESS_STOP)

plt.plot(buffer)
plt.grid(True)
plt.xlabel('x')
plt.ylabel('LSB')
plt.show()

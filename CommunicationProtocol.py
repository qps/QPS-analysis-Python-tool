""" Classes for SPI, I2C and UART communication
    Author: Alexandre Plot (aplot) and Severin Haas
    Date: 21/06/2019 14:30
"""

import time
import numpy as np

################################## Main class ##################################

class CommunicationProtocol:
    """ Base class
    """

    def __init__(self):
        self._temp = None


    def read_register(self, register, nb_byte):
        """ Defined in a subclass
        """
        return None


    def write_register(self, register, value):
        """ Defined in a subclass
        """
        return None


    def read_buffer(self, numregs, numreps, numblocks, data_to_write):
        """ Defined in a subclass
        """
        return None


    def reset(self):
        """ Defined in a subclass
        """
        return None



################################## SPI subclass ##################################
class COMspi(CommunicationProtocol):
    """ This class uses the PyFTDI library
    """

    def __init__(self, spi):
        """ Init function
        """
        self._spi = spi
        super(COMspi, self).__init__()


    def read_register(self, register, nb_byte):
        """ Not implemented yet
        """
        return None


    def write_register(self, register, value):
        """ Not implemented yet
        """
        return None



################################## I2C subclass ##################################
class COMi2c(CommunicationProtocol):
    """ This class uses the PyFTDI library
    """

    def __init__(self, i2c):
        """ Init function
        """
        self._i2c = i2c
        super(COMi2c, self).__init__()


    def read_register(self, register, nb_byte):
        """ Function to read a register
        """
        # Define the byte to send : 01xx xxxx
        masked_register = register + 2**6
        data = self._i2c.exchange([masked_register], nb_byte, True, True)
        return data


    def write_register(self, register, value):
        """ Function to write a register
        """
        # write address : 10xx xxxx + value
        cache = []
        masked_register = register + 2**7
        cache.append(masked_register)
        cache.append(value)
        self._i2c.write(cache, True, True)

        # confirm the writing : 11xx xxxx
        masked_register = register + 2**6 + 2**7
        self._i2c.write([masked_register], True, True)


    def send_command(self, command):
        """ Function to send a command
        """
        self._i2c.write([command], True, True)


    def reset(self):
        """ Function to reset a board by sending 0x3F as a command
        """
        self._i2c.write([0x3f], True, True)



################################## UART subclass ##################################

class COMuart(CommunicationProtocol):
    """ This class uses Pyserial library
    This class is based on uqdsSericomFunctions from Jens Steckert
    """

    def __init__(self, uart, port):
        """ Init function
        """
        # setting all UART parameters for UQDS
        self._uart = uart
        self._uart.baudrate = 2500000
        self._uart.bytesize = 8
        self._uart.timeout = 1
        self._uart.stopbits = 1
        self._uart.port = port
        self._uart.open()
        super(COMuart, self).__init__()


    def reset(self):
        """ Function to reset a board by sending 0x3F as a command
        """
        self.write_register(0, [0x3f, 0, 0, 0])


    def read_register(self, register_address, nb_byte):
        """ Function to read a register
        """
        cmd = 2
        cache = []
        cache.append(cmd)
        cache.append(register_address)
        crc = 0

        for cache_entry in cache:
            crc = crc ^ cache_entry

        cache.append(crc)
        self._uart.write(cache)
        reg_value = self._uart.read(nb_byte)
        return reg_value


    def write_register(self, register_address, value):
        """ Function to write a register
        """
        cmd = 1
        cache = []
        cache.append(cmd)
        cache.append(register_address)
        cache.extend(value)
        crc = 0

        for cache_entry in cache:
            crc = crc ^ cache_entry

        cache.append(crc)
        self._uart.write(cache)
        return cache


    def read_buffer(self, numregs, numreps, numblocks, startreg, stopreg):
        """ Function to read a buffer.
        """
        # Build a byte array containing the necessary bytes to read a range of registers
        content = [0]
        dummybytes = [0, 0, 0, 0, 0]

        for a in range(0, numreps):
            # assemble command vector
            for i in range(startreg, stopreg + 1):
                cache = [2, i]
                xor_cache = 0

                for cache_entry in cache:
                    xor_cache = xor_cache ^ cache_entry

                cache.append(xor_cache)
                # append all cache bytes to content
                for cache_entry in cache:
                    content.append(cache_entry)

                for byte in dummybytes:
                    content.append(byte)

        warr = bytearray(content)

        print('reseting')
        self.reset()
        time.sleep(2)
        print('send recording command')
        self.write_register(0, [0x13, 0, 0, 0])
        time.sleep(2)

        bytes_to_read = numregs * 5 * numreps
        bufferlist = []
        for i in range(numregs):
            bufferlist.append([])

        for block in range(numblocks):
            self._uart.write(warr)
            raw_read_result = bytearray(self._uart.read(bytes_to_read))
            registerlist = [raw_read_result[i:i + 5]
                            for i in range(0, len(raw_read_result), 5)]

            # register map as list
            regmap = []

            # interate through list and check element
            for word in registerlist:
                checks = 0
                # go through the 5 bytes of each word and compare with checksum
                for i in range(0, 4):
                    checks = checks ^ word[i]

                # if checksum is valid, then delete it from word and append to new list
                # if checks == word[4]:
                del word[4]
                regmap.append(int(int.from_bytes(word, byteorder='big', signed=True)))

            # disentangle registermap
            for a in range(numregs):
                bufferlist[a].extend(regmap[a::numregs])

        # Convert to numpy & transpose
        bufferlist_arr = np.transpose(np.asarray(bufferlist))
        bufferlist = []

        return bufferlist_arr

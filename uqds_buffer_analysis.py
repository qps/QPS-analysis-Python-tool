""" Noise characterization on uqds
    Author: Alexandre Plot and Severin Haas
    Date: 30/08/2019 14:30
"""

import time
import matplotlib.pyplot as plt
import numpy as np
import serial
from CommunicationProtocol import COMuart
import noise_analysis as na

SMALL_SIZE = 8
MEDIUM_SIZE = 10
BIGGER_SIZE = 16

plt.rc('font', size=BIGGER_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


# Define the address for reading the buffer
ADRSTART = 112
ADRSTOP = 115
# wcontent = [0]e c
DUMMYBYTES = [0, 0, 0, 0, 0]
REPETITIONS = 256
NUMREGS = (ADRSTOP - ADRSTART) + 1
BLOCKS = 2048
N_SAMPLES = BLOCKS * REPETITIONS
LSB = 2 ** -24
SAMPLIING_FREQUENCY = 754717 # Hz

# define the COM port to use (see device manager setting on windows)
COM = 'COM10'

# Create port for communication
SER = serial.Serial()

# Instanciate an UART communication object with uqds
UQDS = COMuart(SER, COM)

# reset
UQDS.reset()
time.sleep(2)

# changing the gain
# print('gain before:')
# print(UQDS.readRegister(24,5))
UQDS.write_register(24, [0, 0, 0, 0])
time.sleep(2)
UQDS.write_register(28, [0, 0, 0, 0])
time.sleep(2)
UQDS.write_register(32, [0, 0, 0, 0])
time.sleep(2)
# print('gain after:')
# print(UQDS.readRegister(24,5))
# time.sleep(2)

# changing the sampling freq
# print('	GlobalHeartBeat: ')
# print(UQDS.readRegister(23,6))
# time.sleep(2)
UQDS.write_register(23, [0, 0, 0, 0x35])
# time.sleep(2)
# print(UQDS.readRegister(23,6))
time.sleep(2)

# read the buffer
BUFFER = UQDS.read_buffer(NUMREGS, REPETITIONS, BLOCKS, ADRSTART, ADRSTOP)

BUFFER1 = BUFFER[:, 0]
BUFFER2 = BUFFER[:, 1]
BUFFER3 = BUFFER[:, 2]

# Create an array for time
TIME_ARRAY = np.linspace(0, N_SAMPLES / SAMPLIING_FREQUENCY, N_SAMPLES)

# conversion
BUFFER1 = BUFFER1 * LSB
BUFFER2 = BUFFER2 * LSB
BUFFER3 = BUFFER3 * LSB

# Plot the buffer
plt.figure(1)
plt.plot(TIME_ARRAY, BUFFER1)
plt.grid(True)
plt.xlabel('time (s)')
plt.ylabel('Signal (V)')


# Calculate the FFT on channel 1
X, Y = na.calculate_fft(BUFFER1, SAMPLIING_FREQUENCY)
Y = 2.0 / N_SAMPLES * np.abs(Y[0:N_SAMPLES // 2])

# Calculate the FFT on channel 2
X2, Y2 = na.calculate_fft(BUFFER2, SAMPLIING_FREQUENCY)
Y2 = 2.0 / N_SAMPLES * np.abs(Y2[0:N_SAMPLES // 2])

# Calculate the FFT on channel 3
X3, Y3 = na.calculate_fft(BUFFER3, SAMPLIING_FREQUENCY)
Y3 = 2.0 / N_SAMPLES * np.abs(Y3[0:N_SAMPLES // 2])


# Converting into dB
Y_dB = 20 * np.log10(Y)
Y2_dB = 20 * np.log10(Y2)
Y3_dB = 20 * np.log10(Y3)


print('----------------------------------------')
print("Frenquency analysis")

# Plot the FFT of channel 1
plt.figure(2)
plt.plot(X, Y_dB, )
plt.grid(True)
plt.ylabel('Amplitude (dB)')
plt.xlabel('Frequency (Hz)')
plt.title('Channel 1 FFT')

# search for peaks
PEAKS = na.search_peaks(Y_dB, X, -117, 1000)

NUMBER_PEAKS = np.size(PEAKS[0])

# Plot amplitudes and frequencies of peaks
for i in range(1, np.size(PEAKS[0]) + 1):
    print('frequency = ', PEAKS[1][NUMBER_PEAKS - i],
          '\t \t amplitude = ', PEAKS[0][NUMBER_PEAKS - i])
    plt.text(PEAKS[1][i - 1] + 1000, PEAKS[0][i - 1],
             'A = ' + str(round(PEAKS[0][i - 1], 2)) +' dB')
    plt.text(PEAKS[1][i - 1] + 1000, PEAKS[0][i - 1] + 3,
             'f = ' + str(round(PEAKS[1][i - 1] / 1000, 2)) + ' kHz')
plt.plot(PEAKS[1], PEAKS[0], 'r+')


# Plot channel 2
# plt.figure(3)
# plt.plot(X2, Y2_dB)
# # plt.plot(han)
# plt.grid(True)
# plt.ylabel('Amplitude (dB)')
# plt.xlabel('Frequency (Hz)')
# plt.title('Channel 2 FFT')


# Plot channel 3
# plt.figure(4)
# plt.plot(X3, Y3_dB)
# # plt.plot(han)
# plt.grid(True)
# plt.ylabel('Amplitude (dB)')
# plt.xlabel('Frequency (Hz)')
# plt.title('Channel 3 FFT')

print('buffer size = ', np.size(BUFFER1))
print('----------------------------------------')
print("Temporal analysis")
print('standard deviation = ', na.calculate_std_dev(BUFFER1))
print('mean = ', na.calculate_mean(BUFFER1))
print('peak to peak noise = ', na.calculate_pp_noise(BUFFER1))

print(np.mean(Y_dB))

plt.show()
